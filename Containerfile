FROM quay.io/centos/centos:stream9

LABEL com.github.containers.toolbox="true" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image can be used with the toolbox command" \
      summary="Base image for building AutoSD packages" \
      maintainer="Bruce Benson <bbenson@redhat.com>" \
      authors="Leonardo Rossetti, Stephen Smoogen, Lester Claudio, Bruce Benson"

# copy configuration failes to the filesystem
COPY /files rpms.txt /

RUN dnf update -y && \
dnf install -y 'dnf-command(config-manager)' epel-release

# enable crb and autosd repositories
RUN dnf config-manager --set-enabled crb && \
dnf config-manager --add-repo https://buildlogs.centos.org/9-stream/automotive/$(arch)/packages-main/ && \
dnf config-manager --add-repo https://buildlogs.centos.org/9-stream/autosd/$(arch)/packages-main/

# install base packages
RUN dnf -y --skip-broken install $(cat rpms.txt) && \
    dnf clean all
# be able to compile things
RUN dnf -y groupinstall "Development Tools" --exclude=flatpak

# setup rpm environment in /opt/autosd
RUN rpmdev-setuptree && \
mkdir -p /opt/autosd && \
mv /root/rpmbuild /opt/autosd && \
chown -R 1001:0 /opt/autosd

WORKDIR /opt/autosd/rpmbuild
